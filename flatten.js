function flatten(elements){
    let array=[];
    function helper(elements){
        for(let index=0;index<elements.length;index++){
            let single_element=elements[index];
            if(Array.isArray(single_element)){
                helper(single_element);
            }else{
                array.push(single_element);
            }
        }
    }
    helper(elements);
    return array;
}

module.exports=flatten;