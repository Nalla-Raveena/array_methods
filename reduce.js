function reduce(elements,cb,startingValue){
    let accumulator=startingValue;
    for(let index=0;index<elements.length;index++){
        accumulator=cb(accumulator,elements[index])
    }return accumulator
}

module.exports=reduce;